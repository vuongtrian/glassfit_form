import React, { Component } from "react";
import model from "../img/glassesImage/model.jpg";
import "../ReactJS_WebThuKinh/style.css";

class Glasse extends Component {
  arrProduct = [
    {
      id: 1,
      price: 30,
      name: "GUCCI G8850U",
      url: "../img/glassesImage/v1.png",
      desc:
        "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },

    {
      id: 2,
      price: 50,
      name: "GUCCI G8759H",
      url: "../img/glassesImage/v2.png",
      desc:
        "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },

    {
      id: 3,
      price: 30,
      name: "DIOR D6700HQ",
      url: "../img/glassesImage/v3.png",
      desc:
        "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },

    {
      id: 4,
      price: 30,
      name: "DIOR D6005U",
      url: "../img/glassesImage/v4.png",
      desc:
        "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },

    {
      id: 5,
      price: 30,
      name: "PRADA P8750",
      url: "../img/glassesImage/v5.png",
      desc:
        "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },

    {
      id: 6,
      price: 30,
      name: "PRADA P9700",
      url: "../img/glassesImage/v6.png",
      desc:
        "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },

    {
      id: 7,
      price: 30,
      name: "FENDI F8750",
      url: "../img/glassesImage/v7.png",
      desc:
        "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },

    {
      id: 8,
      price: 30,
      name: "FENDI F8500",
      url: "../img/glassesImage/v8.png",
      desc:
        "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },

    {
      id: 9,
      price: 30,
      name: "FENDI F4300",
      url: "../img/glassesImage/v9.png",
      desc:
        "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
  ];

  // glassesImg = g1;

  // state = {
  //   glassesImg: g1,
  //   glassesName: this.arrProduct[0].name,
  //   glassesDesc: this.arrProduct[0].desc,
  // };

  state = {
    selectedGlass: null,
    isShow: false,
  };

  // changeImage = (img, name, des) => () => {
  //   this.setState({
  //         glassesImg: img,
  //         glassesName: name,
  //         glassesDesc: des,
  //   });
  // };

  changeImage = (select) => () => {
    console.log(select);
    this.setState(
      {
        selectedGlass: select,
        isShow: !this.state.isShow,
      },
      () => {
        console.log(this.state.selectedGlass);
      }
    );
  };

  renderGlasses() {
    return this.arrProduct.map((item, index) => {
      return (
        <div
          key={index}
          className="glassesItem"
          onClick={this.changeImage(item)}
        >
          <img src={item.url} alt="glassItem1" />
        </div>
      );
    });
  }

  render() {
    return (
      <div className="containerBox">
        <div className="header">
          <p>TRY GLASSES APP ONLINE</p>
        </div>
        <div className="body">
          <div className="cardList d-flex justify-content-center">
            {/* card 1 */}
            <div className="card1 card" style={{ width: 400 }}>
              <img
                className="card-img-top"
                src={model}
                alt="Model image"
                style={{ width: "100%" }}
              />
              {this.state.isShow && (
                <div className="card-body">
                  <h4 className="card-title">
                    {this.state.selectedGlass.name}
                  </h4>
                  <p className="card-text">{this.state.selectedGlass.desc}</p>
                </div>
              )}

              {this.state.isShow && (
                <div className="glassesFit">
                  <img src={this.state.selectedGlass.url} alt="glassImg" />
                </div>
              )}
            </div>

            {/* card 2 */}
            <div className="card2 card" style={{ width: 400 }}>
              <img
                className="card-img-top"
                src={model}
                alt="Model image"
                style={{ width: "100%" }}
              />
            </div>
          </div>

          <div className="glassesList d-flex ">{this.renderGlasses()};</div>
        </div>
      </div>
    );
  }
}

export default Glasse;
